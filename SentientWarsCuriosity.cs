﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using SharpDX;
using SharpDX.Toolkit;
using SharpDX.XAudio2;
using System;
using System.Collections.Generic;
using Windows.Devices.Sensors;
using Windows.UI.Input;

using System.Diagnostics;


namespace SentientWarsCuriosity
{
    // Use this namespace here in case we need to use Direct3D11 namespace as well, as this
    // namespace will override the Direct3D11.
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;
    using Engine.Object;
    using Gameplay;
    using Engine;

    public class SentientWarsCuriosity : Game
    {
        private GraphicsDeviceManager graphicsDeviceManager;
        private List<GameEntity> models;
        private List<Missile> missiles;
        private SkyBox background;
        private KeyboardManager keyboardManager;
        public KeyboardState keyboardState;
        public GameInput input;
        public AccelerometerReading accelerometerReading;
        public TappedEventArgs tapArgs;

        public SphericalCamera camera;
        public bool started = false;
        public bool pause = false;
        public MainPage mainPage;

        //GAMEPLAY
        public Player humanPlayer;
        public Random rand;
        public TileGrid tileGrid;
        public List<Player> players;
        public List<City> cities;

        public City viewingCity;
        public Planet stationedPlanet;

        public Sound sounds;

        public GameDateTime gdt { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SentientWarsCuriosity" /> class.
        /// </summary>
        public SentientWarsCuriosity(MainPage mainPage)
        {
            // Creates a graphics manager. This is mandatory.
            graphicsDeviceManager = new GraphicsDeviceManager(this);

            // Setup the relative directory to the executable directory
            // for loading contents with the ContentManager
            Content.RootDirectory = "Content";

            // Create the keyboard manager
            keyboardManager = new KeyboardManager(this);
            this.mainPage = mainPage;

            //Create the game input manager
            this.input = new GameInput();

            // initiates list of game entities to be drawn;
            models = new List<GameEntity>();
            players = new List<Player>();
            missiles = new List<Missile>();
            cities = new List<City>();
            // game random number generator.
            rand = new Random(DateTime.Now.Second);

            // Human Player
            humanPlayer = new Player(this, 3, true, "JunMiner", "Jun Min");

            // Initialise event handling.
            input.gestureRecognizer.Tapped += Tapped;
            input.gestureRecognizer.ManipulationStarted += OnManipulationStarted;
            input.gestureRecognizer.ManipulationUpdated += OnManipulationUpdated;
            input.gestureRecognizer.ManipulationCompleted += OnManipulationCompleted;

        }
        
        // called to insert missiles anad cities into the game world.
        public void addModel(GameEntity gameObjects)
        {
            models.Add(gameObjects);
        }

        public void removeModel(GameEntity gameObjects)
        {
            models.Remove(gameObjects);
        }

        public void addMissile(Missile m)
        {
            missiles.Add(m);
        }

        public void removeMissile(Missile m)
        {
            missiles.Remove(m);
        }


        protected override void LoadContent()
        {
            camera = new SphericalCamera(this, 2.0f, 8.0f);
            background = new SkyBox(this, new Vector3(0, 0, 0));
            Planet earth = new Planet(this, new Vector3(0, 0, 0), (float) 1.0, 5, PlanetType.EARTH);
            this.stationedPlanet = earth;
            gdt = new GameDateTime();
            Planet moon = new Planet(this, new Vector3(1.75f, 1.0f, 1.75f), (float) 0.35, 3, PlanetType.MOON);
            Planet sun = new Planet(this, new Vector3(-20.0f, 0f, 0f), (float) 3.0, 4, PlanetType.SUN);
            this.tileGrid = new TileGrid(this, new Vector3(0, 0, 0), earth);
            // NPC PLAYERS
            Player npc1 = new Player(this, 2, false, null, null);
            Player npc2 = new Player(this, 2, false, null, null);

            players.Add(humanPlayer);
            // NPC PLAYERS
            players.Add(npc1);
            players.Add(npc2);

            foreach (Player p in players)
                p.init();

            foreach (Player p in players)
                cities.AddRange(p.getCityList());

            models.Add(earth);            
            models.Add(moon);
            models.Add(sun);

            models.Add(tileGrid);

            sounds = new Sound();
            sounds.LoadWave("Sound/Bathed in the Light.wav", "Intro");
            sounds.LoadWave("Sound/Ambient.wav", "Ambient");
            sounds.LoadWave("Sound/Alarm.wav", "Alarm");
            sounds.LoadWave("Sound/Alert.wav", "Alert");
            sounds.LoadWave("Sound/Explosion.wav", "Explosion");
            sounds.PlayWave("Intro");

            base.LoadContent();
        }

        protected override void Initialize()
        {
            Window.Title = "Sentient Wars: Curiosity";

            base.Initialize();
        }

        protected override void Update(GameTime gameTime)
        {
            if (started && !pause)
            {
                //this.accelerometerReading = input.accelerometer.GetCurrentReading();
                keyboardState = keyboardManager.GetState();

                camera.Update(gameTime);

                mainPage.UpdateNation(humanPlayer);

                for (int i = 0; i < models.Count; i++)
                {
                    models[i].Update(gameTime);
                }

                for (int i = 0; i < missiles.Count; i++)
                {
                    missiles[i].Update(gameTime);
                }
                background.Update(gameTime);
                gdt.update(gameTime);
                this.mainPage.UpdateGameTime();
                // Handle base.Update
                base.Update(gameTime);
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            if (started && !pause)
            {
                // Clears the screen with the Color.CornflowerBlue
                GraphicsDevice.Clear(Color.Black);

                for (int i = 0; i < models.Count; i++)
                {
                    models[i].Draw(gameTime);
                }

                for (int i = 0; i < missiles.Count; i++)
                {
                    missiles[i].Draw(gameTime);
                }
                background.Draw(gameTime);
                // Handle base.Draw
                base.Draw(gameTime);
            }
            
        }

        public void OnManipulationStarted(GestureRecognizer sender, ManipulationStartedEventArgs args)
        {
            // Pass Manipulation events to the game objects.

        }

        public void Tapped(GestureRecognizer sender, TappedEventArgs args)
        {
            // Pass Manipulation events to the game objects.
            switch (this.mainPage.viewMode)
            {
                case MainPage.mode.CITY_VIEW:
                    break;
                case MainPage.mode.TARGET_VIEW:
                    foreach (City c in cities)
                        c.Tapped(sender, args);
                    break;
                case MainPage.mode.WORLD_VIEW:
                    foreach (City c in humanPlayer.getCityList())
                        c.Tapped(sender, args);
                    break;
                default:
                    break;
            }
        }

        public void OnManipulationUpdated(GestureRecognizer sender, ManipulationUpdatedEventArgs args)
        {
            //Scale camera with pinch
            camera.OnManipulationUpdated(sender, args);
            // Update camera position for all game objects
            foreach (var obj in models)
            {
                if (obj.effect != null) { obj.effect.Parameters["View"].SetValue(this.camera.getView()); }

                // TASK 4: Respond to OnManipulationUpdated events for linear motion
                obj.OnManipulationUpdated(sender, args);
            }
        }

        public void OnManipulationCompleted(GestureRecognizer sender, ManipulationCompletedEventArgs args)
        {
        }
    }
}
