﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;

namespace SentientWarsCuriosity.Gameplay
{
    using Engine.Object;
    using Engine.Object.Shape;
    using Engine.Lighting;
    using SharpDX.Toolkit.Graphics;
    using Engine;

    public class City : GameEntity
    {
        private Planet planet;
        public Cube block;
        private Tile tile;
        private float cityRadius;

        public SpriteBatch wordGraphics;
        public int productivity, health, happiness, science, population;
        public float prodRate, healthRate, happyRate, sciRate, popRate;

        public Player owner { get; set; } 
        public int[] inventory { get; set; }
        public string name { get; set; }
        public Units curBuilding { get; set; }
        public int buildingProgress { get; set; }
        public Queue<Missile> inbound { get; set; }
        public Queue<Missile> outbound { get; set; }
       

        public bool select; 

        private Matrix rotationXYZ;

        private static readonly int nINVENTORY = 5;
        private static int cityCount;

        private static readonly string[] cityNames = new[] 
        {
            "Pocoyo City",
            "Starling City",
            "Kampung Attap",
            "Lunar",
            "Duna",
            "Ronanov",
            "SleepyHollow",
            "Minas Tirith",
            "JunMiner",
        };

        public enum Units
        {
            FACTORY,
            SCHOOL,
            HOSPITAL,
            ATTACK,
            EMPTY,
        };
        
        static int[] buildingTime = new int[] {5, 5, 5, 15}; // in game days

        // constructor to found a city :)
        public City(SentientWarsCuriosity game, Player player, Tile tile, int seed)
        {
            this.planet = tile.getPlanet();

            Random rand = new Random(seed);

            this.block = new Cube(player.playerNumber);

            vertices = Buffer.Vertex.New(game.GraphicsDevice, block.getVertices());

            effect = game.Content.Load<Effect>("Phong");
            inputLayout = VertexInputLayout.FromBuffer(0, vertices);
            wordGraphics = new SpriteBatch(game.GraphicsDevice);

            this.population = rand.Next() % 1000000;
            this.popRate = rand.NextFloat(1.00f, 1.10f);

            this.pos = tile.getTileLoc();
            this.owner = player;
            this.tile = tile;
            this.curBuilding = Units.EMPTY;

            this.name = cityNames[rand.Next(0, cityNames.Length)]; 

            this.inventory = new int[nINVENTORY];

            this.productivity = rand.Next(0, 100);
            this.science = rand.Next(0, 100);
            this.health = rand.Next(0, population / 10);
            this.happiness = rand.Next(0, 100);
            for (int i = 0; i < inventory.Length; i++)
            {
                inventory[i] = rand.Next(0, 100);
            }

            this.game = game;
            City.cityCount++;
        }

        public Planet getPlanet()
        {
            return this.planet;
        }

        // GRAPHICS 
        public override void Update(GameTime gameTime)
        {
            // Rotate the cube.
            var time = (float)gameTime.TotalGameTime.TotalSeconds;

            // for city surface orientation.
            Vector3 orientation = tile.getSurfaceNormal();
            Vector3 up = new Vector3(0.0f, 1.0f, 0.0f);
            Vector3 nn = Vector3.Cross(up, orientation - pos);
            float cosMalpha = Vector3.Dot(up, (orientation - pos)) / ((orientation - pos).Length());
            float sinMalpha = (nn.Length()) / ((orientation - pos).Length());
            float alpha = (float)Math.Atan2(sinMalpha, cosMalpha);
            Vector3 axis = nn / nn.Length();
            rotationXYZ = Matrix.RotationAxis(axis, alpha);      

            float scalingFactor = 0.05f + (this.population / 1000000);
            this.cityRadius = scalingFactor + 1.0f;
            world = Matrix.Scaling(scalingFactor) * this.rotationXYZ * Matrix.Translation(pos) * Matrix.RotationY(time / 10);
            worldInverseTranspose = Matrix.Transpose(Matrix.Invert(world));

            // Gameplay Update
            this.health = ((this.health * 10) / this.population) * 100;

        }
        
        // function is called when user selects something to build in city panel
        public bool build(Units unit)
        {
            if (this.curBuilding != Units.EMPTY)
                return false;
            else
            {
                this.curBuilding = unit;
                return true;
            }
        }

        // function is called at every frame.
        void updateBuildingProgress()
        {
            
        }

        
        // PICKING
        public override void Tapped(Windows.UI.Input.GestureRecognizer sender, Windows.UI.Input.TappedEventArgs args)
        {
            float pointX, pointY;
            Matrix inverseViewMatrix, worldInverseMatrix;
            Vector3 direction, origin, rayOrigin, rayDirection;
            bool intersect;

            // Move the mouse cursor coordinates into the -1 to +1 range.
            pointX = ((2.0f * (float)args.Position.X) / (float)game.Window.ClientBounds.Width) - 1.0f;
            pointY = (((2.0f * (float)args.Position.Y) / (float)game.Window.ClientBounds.Height) - 1.0f) * -1.0f;

            // Adjust the points using the projection matrix to account for the aspect ratio of the viewport.
            pointX = pointX / game.camera.getProjection().M11;
            pointY = pointY / game.camera.getProjection().M22;

            // Get the inverse of the view matrix.
            inverseViewMatrix = Matrix.Invert(game.camera.getView());
            // Calculate the direction of the picking ray in view space.
            direction = new Vector3();
            direction.X = (pointX * inverseViewMatrix.M11) + (pointY * inverseViewMatrix.M21) + inverseViewMatrix.M31;
            direction.Y = (pointX * inverseViewMatrix.M12) + (pointY * inverseViewMatrix.M22) + inverseViewMatrix.M32;
            direction.Z = (pointX * inverseViewMatrix.M13) + (pointY * inverseViewMatrix.M23) + inverseViewMatrix.M33;

            // Get the origin of the picking ray which is the position of the camera.
            origin = game.camera.getCameraPos();

            // Get the world matrix and translate to the location of the sphere.
            // STEP SKIPPED as every object has its own world matrix which is constantly updated.

            // Now get the inverse of the translated world matrix.
            worldInverseMatrix = Matrix.Invert(this.world);
           
            // Now transform the ray origin and the ray direction from view space to world space.
            rayOrigin = new Vector3();
            Vector3.TransformCoordinate(ref origin, ref worldInverseMatrix, out rayOrigin);
            rayDirection = new Vector3();
            Vector3.TransformNormal(ref direction, ref worldInverseMatrix, out rayDirection);

            // Normalize the ray direction.
            rayDirection = Vector3.Normalize(rayDirection);

            // Now perform the ray-sphere intersection test.
            intersect = raySphereIntersect(rayOrigin, rayDirection);

            if (intersect)
            {
                selected(game);
            }
            base.Tapped(sender, args);
            return;
        }

        public bool raySphereIntersect(Vector3 rayOrigin, Vector3 rayDirection)
        {
            float a, b, c, discriminant;
            // Calculate the a, b, and c coefficients.
            a = (rayDirection.X * rayDirection.X) + (rayDirection.Y * rayDirection.Y) + (rayDirection.Z * rayDirection.Z);
            b = ((rayDirection.X * rayOrigin.X) + (rayDirection.Y * rayOrigin.Y) + (rayDirection.Z * rayOrigin.Z)) * 2.0f;
            c = ((rayOrigin.X * rayOrigin.X) + (rayOrigin.Y * rayOrigin.Y) + (rayOrigin.Z * rayOrigin.Z)) - (cityRadius * cityRadius);

            // Find the discriminant.
            discriminant = (b * b) - (4 * a * c);

            // if discriminant is negative the picking ray missed the sphere, otherwise it intersected the sphere.
            if (discriminant < 0.0f)
            {
                return false;
            }

            return true;

        }

        public void selected(SentientWarsCuriosity game)
        {
            this.select = true;
            if (game.mainPage.viewMode == MainPage.mode.WORLD_VIEW)
                game.mainPage.CityView(this);
            // target selected, proceeding to missile launch.
            if (game.mainPage.viewMode == MainPage.mode.TARGET_VIEW)
            {
                if (game.viewingCity.inventory[(int)Units.ATTACK] == 0)
                    game.mainPage.promptInvalidTarget(MainPage.InvalidTargetError.LACK_MISSILE);
                else if (this.owner == game.viewingCity.owner)
                    game.mainPage.promptInvalidTarget(MainPage.InvalidTargetError.SUICIDE);
                else
                {
                    game.viewingCity.inventory[(int)Units.ATTACK]--;
                    Missile.launchMissile(game, game.viewingCity, this);
                    game.mainPage.NationView(null, null);
                }
            }
        }

        // when inbound missile on flight, this is called by missile manager to resolve if the missile is intercepted.
        bool missileIntercept(Missile[] incoming)
        {
            Random rand = new Random(System.DateTime.Now.Millisecond);
            if (rand.Next() % 2 == 0)
                // miss interception
                return false;
            else
                // interception is successful.
                return true;
        }

        // called when new building is constructed.
        void addModifier(Units unit)
        {
            switch (unit)
            {
                case Units.FACTORY:
                    this.productivity += 10;
                    this.health -= 10;
                    break;
                case Units.HOSPITAL:
                    this.health += 10;
                    break;
                case Units.SCHOOL:
                    this.science += 10;
                    break;
            }
        }

        
        // ACCESSORS to city information!
     
        // called when city gets nuked.
        public void gotNuked()
        {
            game.sounds.PlayWave("Alert");
            Random rand = new Random(System.DateTime.Now.Millisecond);
            // max damage to building -50%
            // min damage to building -25%
            // max population casualties -50%
            // min casualties - 25%
            // health = 0%
            // unhappiness -40%
            this.healthRate = -0.5f;

            
        }

        public Tile getTile()
        {
            return this.tile;
        }

        public string getName()
        {
            return this.name;
        }

  
    }

}
