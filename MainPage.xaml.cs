﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using SharpDX;

namespace SentientWarsCuriosity
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 
    using Gameplay;
    using Engine;
    using Engine.Object;

    public sealed partial class MainPage
    {
        private readonly SentientWarsCuriosity game;

        public enum mode
        {
            CITY_VIEW,
            WORLD_VIEW,
            TARGET_VIEW,
            BUILD_VIEW,
        }

        public mode viewMode;

        public struct HUDInfo
        {
            public int happiness;
            public int health;
            public int pop;
            public int productivity;
            public int science;

        }

        public MainPage()
        {
            InitializeComponent();
            game = new SentientWarsCuriosity(this);
            game.Run(this);
        }

        private void StartGame(object sender, RoutedEventArgs e)
        {
            game.started = true;
            mainPage.Visibility = Visibility.Collapsed;
            InGame.Visibility = Visibility.Visible;
            Nation.Visibility = Visibility.Visible;
            City.Visibility = Visibility.Collapsed;
            this.viewMode = mode.WORLD_VIEW;
        }

        private void PauseGame(object sender, RoutedEventArgs e)
        {
            game.pause = true;
            mainPage.Visibility = Visibility.Visible;
            Resume.Visibility = Visibility.Visible;
            cmdNewGame.Visibility = Visibility.Collapsed;
            InGame.Visibility = Visibility.Collapsed;
        }
        private void ResumeGame(object sender, RoutedEventArgs e)
        {
            game.pause = false;
            mainPage.Visibility = Visibility.Collapsed;
            Resume.Visibility = Visibility.Collapsed;
            InGame.Visibility = Visibility.Visible;
        }
        private void ExitGame(object sender, RoutedEventArgs e)
        {
            Application.Current.Exit();
        }
        public void UpdateGameTime()
        {
            GameTime.Text = game.gdt.time + " " + game.gdt.day + " " + game.gdt.month + " " +
                game.gdt.year;

            CGameTime.Text = game.gdt.time + " " + game.gdt.day + " " + game.gdt.month + " " +
                game.gdt.year;
        }

        public void UpdateCity(City city)
        {
            CityName.Text = city.getName();
            CityPopulation.Text = city.population.ToString();
            CityProductivity.Text = city.productivity.ToString();
            CityHealthBar.Value = city.health;
            CityHappyBar.Value = city.happiness;
            CityNation.Text = city.owner.getNationName();
            CityMissile.Text = city.inventory[(int) Gameplay.City.Units.ATTACK].ToString();
            CityLivIndexBar.Value = game.stationedPlanet.livability;

            buildProgress.Value = city.buildingProgress;
            switch (city.curBuilding)
            {
                case Gameplay.City.Units.ATTACK:
                    ActiveBuildMissile.Visibility = Visibility.Visible;
                    ActiveBuildNothing.Visibility = Visibility.Collapsed;
                    break;
                case Gameplay.City.Units.EMPTY:
                    ActiveBuildAcademy.Visibility = Visibility.Collapsed;
                    ActiveBuildFactory.Visibility = Visibility.Collapsed;
                    ActiveBuildHospital.Visibility = Visibility.Collapsed;
                    ActiveBuildMissile.Visibility = Visibility.Collapsed;
                    ActiveBuildNothing.Visibility = Visibility.Visible;
                    break;
                case Gameplay.City.Units.FACTORY:
                    ActiveBuildFactory.Visibility = Visibility.Visible;
                    ActiveBuildNothing.Visibility = Visibility.Collapsed;
                    break;
                case Gameplay.City.Units.HOSPITAL:
                    ActiveBuildHospital.Visibility = Visibility.Visible;
                    ActiveBuildNothing.Visibility = Visibility.Collapsed;
                    break;
                case Gameplay.City.Units.SCHOOL:
                    ActiveBuildAcademy.Visibility = Visibility.Visible;
                    ActiveBuildNothing.Visibility = Visibility.Collapsed;
                    break;
            }

        }

        public void UpdateNation(Player p)
        {
            NationName.Text = p.getNationName();
            GovName.Text = p.govModifier.name;
            int totalPop = 0, totalHappiness = 0, totalHealth = 0, livability = 0, totalProduction = 0;
            foreach (City c in p.getCityList())
            {
                totalPop += c.population;
                totalHappiness += c.happiness;
                totalHealth += c.health;
                totalProduction += c.productivity;
            }

            int meanHappiness = (int)(totalHappiness / p.getCityList().Count);
            int meanHealth = (int)(totalHealth / p.getCityList().Count);
            
            NationPopulation.Text = totalPop.ToString();
            NationProductivity.Text = totalProduction.ToString();
            NationHappyBar.Value = meanHappiness;
            NationHealthBar.Value = meanHealth;
            NationLivIndexBar.Value = game.stationedPlanet.livability;
        }

        public void NationView(object sender, RoutedEventArgs e)
        {
            Nation.Visibility = Visibility.Visible;
            City.Visibility = Visibility.Collapsed;
            Target.Visibility = Visibility.Collapsed;
            this.viewMode = mode.WORLD_VIEW;
        }

        public void CityView(City city)
        {
            game.viewingCity = city;
            UpdateCity(city);
            Nation.Visibility = Visibility.Collapsed;
            City.Visibility = Visibility.Visible;
            Target.Visibility = Visibility.Collapsed;
            this.viewMode = mode.CITY_VIEW;
        }

        public void CityView(object Sender, RoutedEventArgs e)
        {
            Button b = Sender as Button;
            if (b.Name.Equals("buildMenuReturn"))
                BuildMenu.Visibility = Visibility.Collapsed;
            if (b.Name.Equals("invalidReturn"))
                invalidViewPrompt.Visibility = Visibility.Collapsed;
            UpdateCity(game.viewingCity);
            City.IsTapEnabled = true;
        }

        // TARGET VIEW
        public void TargetView(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            if (b.Name.Equals("TargetErrorReturn"))
            {
                TargetErrorBox.Visibility = Visibility.Collapsed;
                CityView(game.viewingCity);
            }
            else
            {
                this.viewMode = mode.TARGET_VIEW;
                MissileCount.Text = game.viewingCity.inventory[(int)Gameplay.City.Units.ATTACK].ToString();
                City.Visibility = Visibility.Collapsed;
                Nation.Visibility = Visibility.Collapsed;
                Target.Visibility = Visibility.Visible;
            }
      
        }

        public enum InvalidTargetError
        {
            SUICIDE,
            LACK_MISSILE,
            PROXIMITY,
        }

        public void promptInvalidTarget(InvalidTargetError error)
        {
            switch (error)
            {
                case InvalidTargetError.LACK_MISSILE:
                    TargetErrorMsg.Text = "You don't have enough Nuclear Missiles";
                    break;
                case InvalidTargetError.PROXIMITY:
                    TargetErrorMsg.Text = "Target city is too close. Danger!";
                    break;
                case InvalidTargetError.SUICIDE:
                    TargetErrorMsg.Text = "Sorry " + game.humanPlayer.getPlayerName() + ", nuking your people is forbidden.";
                    break;
                default:
                    TargetErrorMsg.Text = "ERROR: Debug";
                    break;
            }
            TargetErrorBox.Visibility = Visibility.Visible;
            this.viewMode = mode.CITY_VIEW;
        }

        // IN BUILD MODE
        public void buildView(object sender, RoutedEventArgs e)
        {
            this.viewMode = mode.BUILD_VIEW;
            hospitalCount.Text = game.viewingCity.inventory[0].ToString();
            academyCount.Text = game.viewingCity.inventory[1].ToString();
            factoryCount.Text = game.viewingCity.inventory[2].ToString();
            BuildMenu.Visibility = Visibility.Visible;
        }

        public void promptInvalidBuild()
        {
            invalidViewPrompt.Visibility = Visibility.Visible;
            BuildMenu.Visibility = Visibility.Collapsed;
        }

        public void build(object Sender, RoutedEventArgs e)
        {
            Button b = Sender as Button;
            if (b.Name.Equals("BuildHospital"))
                if (!game.viewingCity.build(Gameplay.City.Units.HOSPITAL))
                    promptInvalidBuild();
            if (b.Name.Equals("BuildMissile"))
                if (!game.viewingCity.build(Gameplay.City.Units.ATTACK))
                    promptInvalidBuild();
            if (b.Name.Equals("BuildFactory"))
                if (!game.viewingCity.build(Gameplay.City.Units.FACTORY))
                    promptInvalidBuild();
            if (b.Name.Equals("BuildAcademy"))
                if (!game.viewingCity.build(Gameplay.City.Units.SCHOOL))
                    promptInvalidBuild();
            UpdateCity(game.viewingCity);
        }
    }
}
