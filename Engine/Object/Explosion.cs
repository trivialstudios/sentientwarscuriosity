﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;
using System.Diagnostics;

namespace SentientWarsCuriosity.Engine.Object
{

    using SharpDX.Toolkit.Graphics;
    using Engine.Object.Shape;
    using Engine.Object;
    using Gameplay;

    class Explosion : GameEntity
    {
        Icosahedron ico;
        
        public Explosion(SentientWarsCuriosity game, Vector3 startpos)
        {

            ico = new Icosahedron(1);

            pos = startpos;

            lightsrcpos = new Vector3(-20, 0, 0);
            lightsrc = lightsrcpos;

            vertices = Buffer.Vertex.New(game.GraphicsDevice, ico.generateVertices());
            effect = game.Content.Load<Effect>("Moon");
            inputLayout = VertexInputLayout.FromBuffer(0, vertices);
            this.game = game;
        }

        public override void Update(GameTime gameTime)
        {
            float time = (float)gameTime.TotalGameTime.TotalSeconds;
            world = Matrix.Scaling(0.2f) * Matrix.Translation(pos) * Matrix.RotationY(time/10);
            worldInverseTranspose = Matrix.Transpose(Matrix.Invert(world));
            lightsrc = (Matrix.Translation(lightsrcpos) * Matrix.RotationY(-time * 0.1f)).TranslationVector;
        }
    }
}
