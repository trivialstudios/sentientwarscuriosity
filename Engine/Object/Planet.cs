﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;
using System.Diagnostics;

namespace SentientWarsCuriosity
{
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;
    using Engine.Object;
    using Engine.Lighting;
    using Engine.Object.Shape;

    public class Planet : GameEntity
    {
        public float size;
        public int livability { get; set; }
        private readonly int MAXlivability = 100;
        private readonly int LOWLivability = 10;
        private PlanetType type;

        private QuadSphere qs;

        public Planet(SentientWarsCuriosity game, Vector3 startpos, float setsize, int settess, PlanetType settype)
        {           
            //Icosahedron ico = new Icosahedron(4);

            size = setsize;

            type = settype;

            qs = new QuadSphere(settess, size, type);

            pos = startpos;

            lightsrcpos = new Vector3(-20, 0, 0);
            lightsrc = lightsrcpos;

            //vertices = Buffer.Vertex.New(game.GraphicsDevice, ico.generateVertices());
            vertices = Buffer.Vertex.New(game.GraphicsDevice, qs.generateVertices());

            if (type.Equals(PlanetType.SUN))
                effect = game.Content.Load<Effect>("Sun");
            else if (type.Equals(PlanetType.MOON))
                effect = game.Content.Load<Effect>("Moon");
            else
                effect = game.Content.Load<Effect>("Earth");


            inputLayout = VertexInputLayout.FromBuffer(0, vertices);
            this.game = game;
        }
        
        public override void Update(GameTime gameTime)
        {
            // Rotate the cube.
            var time = (float)gameTime.TotalGameTime.TotalSeconds;
            if (type.Equals(PlanetType.MOON))
            {
                world = Matrix.RotationX(0.2f) * Matrix.RotationY(time * 0.2f) * Matrix.RotationZ(0.2f) * Matrix.Translation(pos) * Matrix.RotationY(time * 0.3f);
            }
            else if (type.Equals(PlanetType.SUN))
            {
                world = Matrix.Translation(pos) * Matrix.RotationY(-time * 0.1f);
            }
            else
            {
                world = Matrix.Translation(pos) * Matrix.RotationY(time / 10);
            }
            worldInverseTranspose = Matrix.Transpose(Matrix.Invert(world));
            lightsrc = (Matrix.Translation(lightsrcpos) * Matrix.RotationY(-time * 0.1f)).TranslationVector;
        }

        public QuadSphere getQuadSphere()
        {
            return qs;
        }
    }

    // declares the enum
    public enum PlanetType
    {
        EARTH,
        MOON,
        SUN
    }
}
