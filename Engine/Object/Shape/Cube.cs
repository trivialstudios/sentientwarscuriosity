﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Direct3D11;
using SharpDX.Toolkit;
using SharpDX;

namespace SentientWarsCuriosity.Engine.Object.Shape
{

    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;

    using Engine.Lighting;

    public class Cube
    {
        VertexPositionNormalColor[] vertices;

        public static readonly Color[] PlayerColor = new[] {
            Color.AliceBlue,
            Color.Blue,
            Color.OrangeRed,
            Color.Fuchsia,
            Color.Lavender,
            Color.Black,
            Color.White,
        };

        public static readonly Vector3[] surface = new[] {
            new Vector3(0.0f, 0.0f, -1.0f),
            new Vector3(0.0f, 0.0f, 1.0f),
            new Vector3(0.0f, 1.0f, 0.0f),
            new Vector3(0.0f, -1.0f, 0.0f),
            new Vector3(-1.0f, 0.0f, 0.0f),
            new Vector3(1.0f, 0.0f, 0.0f),
        };

        public Cube(int player)
        {
            Vector3 frontNormal = new Vector3(0.0f, 0.0f, -1.0f);
            Vector3 backNormal = new Vector3(0.0f, 0.0f, 1.0f);
            Vector3 topNormal = new Vector3(0.0f, 1.0f, 0.0f);
            Vector3 bottomNormal = new Vector3(0.0f, -1.0f, 0.0f);
            Vector3 leftNormal = new Vector3(-1.0f, 0.0f, 0.0f);
            Vector3 rightNormal = new Vector3(1.0f, 0.0f, 0.0f);

            vertices = new []
            {
                new VertexPositionNormalColor(new Vector3(-1.0f, -1.0f, -1.0f), frontNormal, PlayerColor[player]), // Front
                new VertexPositionNormalColor(new Vector3(-1.0f, 1.0f, -1.0f), frontNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, 1.0f, -1.0f), frontNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, -1.0f, -1.0f), frontNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, 1.0f, -1.0f), frontNormal,PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, -1.0f, -1.0f), frontNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, -1.0f, 1.0f), backNormal, PlayerColor[player]), // BACK
                new VertexPositionNormalColor(new Vector3(1.0f, 1.0f, 1.0f), backNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, 1.0f, 1.0f), backNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, -1.0f, 1.0f), backNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, -1.0f, 1.0f), backNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, 1.0f, 1.0f), backNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, 1.0f, -1.0f), topNormal, PlayerColor[player]), // Top
                new VertexPositionNormalColor(new Vector3(-1.0f, 1.0f, 1.0f), topNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, 1.0f, 1.0f), topNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, 1.0f, -1.0f), topNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, 1.0f, 1.0f), topNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, 1.0f, -1.0f), topNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, -1.0f, -1.0f), bottomNormal, PlayerColor[player]), // Bottom
                new VertexPositionNormalColor(new Vector3(1.0f, -1.0f, 1.0f), bottomNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, -1.0f, 1.0f), bottomNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, -1.0f, -1.0f),bottomNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, -1.0f, -1.0f), bottomNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, -1.0f, 1.0f), bottomNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, -1.0f, -1.0f), leftNormal, PlayerColor[player]), // Left
                new VertexPositionNormalColor(new Vector3(-1.0f, -1.0f, 1.0f), leftNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, 1.0f, 1.0f), leftNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, -1.0f, -1.0f), leftNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, 1.0f, 1.0f), leftNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(-1.0f, 1.0f, -1.0f), leftNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, -1.0f, -1.0f), rightNormal, PlayerColor[player]), // Right
                new VertexPositionNormalColor(new Vector3(1.0f, 1.0f, 1.0f), rightNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, -1.0f, 1.0f), rightNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, -1.0f, -1.0f), rightNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, 1.0f, -1.0f), rightNormal, PlayerColor[player]),
                new VertexPositionNormalColor(new Vector3(1.0f, 1.0f, 1.0f), rightNormal, PlayerColor[player]),
            };
        }

        public VertexPositionNormalColor[] getVertices()
        {
            return vertices;
        }

    }
}


