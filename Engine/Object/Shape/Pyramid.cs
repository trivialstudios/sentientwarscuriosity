﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;

namespace SentientWarsCuriosity.Engine.Object.Shape
{
    using Engine.Lighting;
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;
    

    class Pyramid
    {
        private VertexPositionNormalColor[] vertices;

        public Pyramid()
        {
            Vector3 bottomNormal = new Vector3(0.0f, -1.0f, 0.0f);
            Vector3 frontNormal = new Vector3(0.0f, 1.0f, -1.0f);
            Vector3 rightNormal = new Vector3(1.0f, 1.0f, 0.0f);
            Vector3 leftNormal = new Vector3(-1.0f,  1.0f, 0.0f);
            Vector3 backNormal = new Vector3(0.0f, 1.0f, 1.0f);
            vertices = new []
            {
                // bottom
                new VertexPositionNormalColor(new Vector3(1.0f, 0.0f, -1.0f), bottomNormal, Color.Red),
                new VertexPositionNormalColor(new Vector3(1.0f, 0.0f, 1.0f), bottomNormal, Color.Red),
                new VertexPositionNormalColor(new Vector3(-1.0f, 0.0f, 1.0f), bottomNormal, Color.Red),
                new VertexPositionNormalColor(new Vector3(1.0f, 0.0f, -1.0f), bottomNormal, Color.Red),
                new VertexPositionNormalColor(new Vector3(-1.0f, 0.0f, 1.0f), bottomNormal, Color.Red),
                new VertexPositionNormalColor(new Vector3(-1.0f, 0.0f, -1.0f), bottomNormal, Color.Red),
                //front
                new VertexPositionNormalColor(new Vector3(-1.0f, 0.0f, -1.0f), frontNormal, Color.OrangeRed),
                new VertexPositionNormalColor(new Vector3(0.0f, 4.0f, 0.0f), frontNormal, Color.OrangeRed),
                new VertexPositionNormalColor(new Vector3(1.0f, 0.0f, -1.0f), frontNormal, Color.OrangeRed),
                //right
                new VertexPositionNormalColor(new Vector3(1.0f, 0.0f, -1.0f), rightNormal, Color.OrangeRed),
                new VertexPositionNormalColor(new Vector3(0.0f, 4.0f, 0.0f), rightNormal, Color.OrangeRed),
                new VertexPositionNormalColor(new Vector3(1.0f, 0.0f, 1.0f), rightNormal, Color.OrangeRed),
                //left
                new VertexPositionNormalColor(new Vector3(-1.0f, 0.0f, 1.0f), leftNormal, Color.OrangeRed),
                new VertexPositionNormalColor(new Vector3(0.0f, 4.0f, 0.0f), leftNormal, Color.OrangeRed),
                new VertexPositionNormalColor(new Vector3(-1.0f, 0.0f, -1.0f), leftNormal, Color.OrangeRed),
                //back
                new VertexPositionNormalColor(new Vector3(1.0f, 0.0f, 1.0f), backNormal, Color.OrangeRed),
                new VertexPositionNormalColor(new Vector3(0.0f, 4.0f, 0.0f), backNormal, Color.OrangeRed),
                new VertexPositionNormalColor(new Vector3(-1.0f, 0.0f, 1.0f), backNormal, Color.OrangeRed),
            };
        }

        public VertexPositionNormalColor[] getVertices()
        {
            return vertices;
        }
    }
}
