﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;
using System.Diagnostics;

namespace SentientWarsCuriosity.Engine.Object.Shape
{
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;

    public class Landscape
    {
        public float[,] heightMap;

        // Summary:
        //  detailLevel, level of detail
        //  pixelSize, the dimensions of the world in pixels 
        public int nVertices;

        private float originalRange;
        private float initValue;
        private float range;
        public float habitableLand;
        private static Random random;
        

        public Landscape(int size)
        {
            this.nVertices = (int)Math.Pow(2.0d, (double)size) + 1;
            this.originalRange = (nVertices - 1) / 4;
            this.habitableLand = 0.1f * this.originalRange;
            range = this.originalRange;

            //GROUND VALUE
            initValue = float.MaxValue;

            // Step 1. Height Map
            // Initialization
            heightMap = new float[nVertices, nVertices];
            for (int i = 0; i < nVertices; i++)
            {
                for (int k = 0; k < nVertices; k++)
                {
                    heightMap[i, k] = initValue;
                }
            }

            // Seeding the corners
            random = new Random();

            for (int i = 0; i < nVertices; i++)
            {
                for (int k = 0; k < nVertices; k++)
                {
                    heightMap[i, k] = initValue;
                }
            }

            heightMap[0, 0] = random.NextFloat(-range * 0.6f, range * 0.6f);
            heightMap[0, nVertices - 1] = random.NextFloat(-range * 0.6f, range * 0.6f);
            heightMap[nVertices - 1, 0] = random.NextFloat(-range * 0.6f, range * 0.6f);
            heightMap[nVertices - 1, nVertices - 1] = random.NextFloat(-range * 0.6f, range * 0.6f);
            
        }

        public void generate()
        {
            // Init
            bool[] isVisited = new bool[nVertices];
            for (int i = 0; i < nVertices; i++)
                isVisited[i] = false;

            int squareLength;
            // Generating the height map
            while ((squareLength = getSquareLength(isVisited, nVertices)) > 1)
            {
                // perform diamond step and square step;
                heightMap = diamondStep(heightMap, nVertices, initValue, squareLength, range);
                heightMap = squareStep(heightMap, nVertices, initValue, squareLength, range);
                isVisited = updateVisted(isVisited, nVertices, squareLength);
                range *= 0.5f;
            }
        }

        public Color heightcol(float height, PlanetType type)
        {
            float range = originalRange;

            if (type.Equals(PlanetType.EARTH))
            {
                    if (height < -(0.5 * range))
                    {
                        return Color.DarkBlue;
                    }
                    else if (height < -(0.2 * range))
                    {
                        return Color.Blue;
                    }
                    else if (height < -(0.1 * range))
                    {
                        return Color.LightSkyBlue;
                    }
                    else if (height < 0.1 * range)
                    {
                        return Color.LawnGreen;
                    }
                    else if (height < 0.4 * range)
                    {
                        return Color.ForestGreen;
                    }
                    else if (height < 0.6 * range)
                    {
                        return Color.Brown;
                    }
                    else
                    {
                        return Color.Snow;
                    }
            }
            else if (type.Equals(PlanetType.MOON))
            {
                if (height < -(0.3 * range))
                {
                    return Color.DarkGray;
                }
                else if (height < -(0.2 * range))
                {
                    return Color.Gray;
                }
                else if (height < -(0.1 * range))
                {
                    return Color.SlateGray;
                }
                else if (height < 0.5 * range)
                {
                    return Color.GhostWhite;
                }
                else
                {
                    return Color.Silver;
                }
            }
            else
            {
                if (height < -(0.4 * range))
                {
                    return Color.DarkOrange;
                }
                else if (height < -(0.3 * range))
                {
                    return Color.Orange;
                }
                else if (height < 0.1 * range)
                {
                    return Color.Red;
                }
                else if (height < 0.5 * range)
                {
                    return Color.OrangeRed;
                }
                else
                {
                    return Color.Gold;
                }
            }

        }

        public float seaCheck(float height)
        {
            float range = originalRange;

            if (height < -(0.2 * range))
            {
                return (-0.2f * range);
            }
            return height;
        }

        public static int getSquareLength(bool[] isVisted, int nVertices)
        {
            int length = 0;
            for (int i = 1; i < nVertices; i++)
            {
                length++;
                if (isVisted[i])
                    break;
            }
            return length;
        }

        public static bool[] updateVisted(bool[] isVisited, int nVertices, int oldSquareLength)
        {
            int newSquareLength = oldSquareLength / 2;
            for (int i = 0; i < nVertices; i = i + newSquareLength)
                isVisited[i] = true;

            return isVisited;
        }

        public static float[,] diamondStep(float[,] heightMap, int nVertices, float initValue, int squareLength, float range)
        {
            int newX = 0, newZ = 0;
            float corner1, corner2, corner3, corner4;
            float averageHeight;

            // find top-left corners of new sub-squares.
            for (int i = 0; i < nVertices - 1; i = i + squareLength)
            {
                for (int k = 0; k < nVertices - 1; k = k + squareLength)
                {
                    corner1 = heightMap[i, k];
                    corner2 = heightMap[i, k + squareLength];
                    corner3 = heightMap[i + squareLength, k];
                    corner4 = heightMap[i + squareLength, k + squareLength];

                    averageHeight = ((corner1 + corner2 + corner3 + corner4) / 4);

                    newX = i + squareLength / 2;
                    newZ = k + squareLength / 2;
                    heightMap[newX, newZ] = averageHeight + random.NextFloat(-range, range);
                }
            }

            return heightMap;
        }

        public static float[,] squareStep(float[,] heightMap, int nVertices, float initValue, int squareLength, float range)
        {
            int newSquareLength = squareLength / 2;

            for (int i = 0; i < nVertices; i = i + newSquareLength)
            {
                for (int k = 0; k < nVertices; k = k + newSquareLength)
                {
                    float corner1 = 0, corner2 = 0, corner3 = 0, corner4 = 0;
                    float averageHeight;
                    int nCorners = 0;

                    if (heightMap[i, k] == initValue)
                    {
                        if (k - newSquareLength >= 0)
                        {
                            corner1 = heightMap[i, k - newSquareLength];
                            nCorners++;
                        }

                        if (k + newSquareLength < nVertices)
                        {
                            corner2 = heightMap[i, k + newSquareLength];
                            nCorners++;
                        }

                        if (i - newSquareLength >= 0)
                        {
                            corner3 = heightMap[i - newSquareLength, k];
                            nCorners++;
                        }

                        if (i + newSquareLength < nVertices)
                        {
                            corner4 = heightMap[i + newSquareLength, k];
                            nCorners++;
                        }

                        averageHeight = (corner1 + corner2 + corner3 + corner4) / nCorners;

                        heightMap[i, k] = averageHeight + random.NextFloat(-range, range);
                    }

                }
            }

            return heightMap;
        }

        public static float newHeight(float height, float range)
        {
            if (height < (-0.4f * range))
                return -0.4f * range;
            else
                return height;
        }
        
        public float[] getSide(Side side)
        {
            float[] heights = new float[nVertices];

            for (int i = 0; i < nVertices; i++)
            {
                switch (side)
                {
                    case Side.TOP:
                        heights[i] = heightMap[i, nVertices - 1];
                        break;
                    case Side.BOTTOM:
                        heights[i] = heightMap[i, 0];
                        break;
                    case Side.LEFT:
                        heights[i] = heightMap[0, i];
                        break;
                    case Side.RIGHT:
                        heights[i] = heightMap[nVertices - 1, i];
                        break;
                }
            }
            return heights;
        }

        public void setSide(Side side, float[] newside)
        {
            for (int i = 0; i < nVertices; i++)
            {
                switch (side)
                {
                    case Side.TOP:
                        heightMap[i, nVertices - 1] = newside[i];
                        break;
                    case Side.BOTTOM:
                        heightMap[i, 0] = newside[i];
                        break;
                    case Side.LEFT:
                        heightMap[0, i] = newside[i];
                        break;
                    case Side.RIGHT:
                        heightMap[nVertices - 1, i] = newside[i];
                        break;
                }
            }
        }

        public float percentWater()
        {
            float range = originalRange;

            float above = 0;
            float below = 0;

            for (int i = 0; i < nVertices; i++)
            {
                for (int j = 0; j < nVertices; j++)
                {
                    if (heightMap[i, j] < -(0.2 * range))
                    {
                        below += 1;
                    }
                    else
                    {
                        above += 1;
                    }
                }
            }

            return (above/(above + below));
        }
        
    }

    // declares the enum
    public enum Side
    {
        TOP,
        BOTTOM,
        LEFT,
        RIGHT
    }
}
