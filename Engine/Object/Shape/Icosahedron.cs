﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;

using System.Diagnostics;

namespace SentientWarsCuriosity.Engine.Object.Shape
{
    using SharpDX.Toolkit.Graphics;
    using Engine.Lighting;

    class Icosahedron
    {
        // raw_icosqueue 
        public Queue<Tuple<Vector3, Vector3, Vector3>> icosqueue = new Queue<Tuple<Vector3, Vector3, Vector3>>();
        public int tessellationlvl = 0;

        public Icosahedron(int tess)
        {
            // Generate Icosahedron
            Vector3[] icosaVertices = new Vector3[12];
            double theta = 26.56505117707799 * Math.PI / 180.0;
            double stheta = Math.Sin(theta);
            double ctheta = Math.Cos(theta);

            icosaVertices[0] = new Vector3(0, 0, -1.0f);

            // Lower Pentagon
            double phi = Math.PI / 5;
            for (int i = 1; i < 6; i++)
            {
                icosaVertices[i] = new Vector3((float)(ctheta * Math.Cos(phi)), (float)(ctheta * Math.Sin(phi)), (float)(-1 * stheta));
                phi += 2 * Math.PI / 5;
            }

            // Upper Pentagon
            phi = 0;
            for (int i = 6; i < 11; i++)
            {
                icosaVertices[i] = new Vector3((float)(ctheta * Math.Cos(phi)), (float)(ctheta * Math.Sin(phi)), (float)stheta);
                phi += 2 * Math.PI / 5;
            }

            icosaVertices[11] = new Vector3(0, 0, 1.0f);

            icosqueue.Enqueue(Tuple.Create(icosaVertices[0], icosaVertices[2], icosaVertices[1]));

            icosqueue.Enqueue(Tuple.Create(icosaVertices[0], icosaVertices[3], icosaVertices[2]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[0], icosaVertices[4], icosaVertices[3]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[0], icosaVertices[5], icosaVertices[4]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[0], icosaVertices[1], icosaVertices[5]));

            icosqueue.Enqueue(Tuple.Create(icosaVertices[1], icosaVertices[2], icosaVertices[7]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[2], icosaVertices[3], icosaVertices[8]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[3], icosaVertices[4], icosaVertices[9]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[4], icosaVertices[5], icosaVertices[10]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[5], icosaVertices[1], icosaVertices[6]));

            icosqueue.Enqueue(Tuple.Create(icosaVertices[1], icosaVertices[7], icosaVertices[6]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[2], icosaVertices[8], icosaVertices[7]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[3], icosaVertices[9], icosaVertices[8]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[4], icosaVertices[10], icosaVertices[9]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[5], icosaVertices[6], icosaVertices[10]));

            icosqueue.Enqueue(Tuple.Create(icosaVertices[6], icosaVertices[7], icosaVertices[11]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[7], icosaVertices[8], icosaVertices[11]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[8], icosaVertices[9], icosaVertices[11]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[9], icosaVertices[10], icosaVertices[11]));
            icosqueue.Enqueue(Tuple.Create(icosaVertices[10], icosaVertices[6], icosaVertices[11]));

            tessellate(tess);
        }

        public VertexPositionNormalColor[] generateVertices()
        {
            // Number of Vertices depend on level of tesselation
            VertexPositionNormalColor[] icoVPC = new VertexPositionNormalColor[icosqueue.Count * 3];

            // TODO
            for (int i = 0; i < icosqueue.Count; i++)
            {
                Vector3 normal = Vector3.Cross(icosqueue.ElementAt(i).Item2 - icosqueue.ElementAt(i).Item1, icosqueue.ElementAt(i).Item3 - icosqueue.ElementAt(i).Item1);
                icoVPC[i * 3 + 0] = new VertexPositionNormalColor(icosqueue.ElementAt(i).Item3, icosqueue.ElementAt(i).Item3, Color.Red);
                icoVPC[i * 3 + 1] = new VertexPositionNormalColor(icosqueue.ElementAt(i).Item2, icosqueue.ElementAt(i).Item2, Color.Orange);
                icoVPC[i * 3 + 2] = new VertexPositionNormalColor(icosqueue.ElementAt(i).Item1, icosqueue.ElementAt(i).Item1, Color.Gold);
            }
            return icoVPC;
        }

        private void tessellate(int tesselationLevel)
        {
            Tuple<Vector3, Vector3, Vector3> face;
            Vector3 v1;
            Vector3 v2;
            Vector3 v3;
            Vector3 v4;
            Vector3 v5;
            Vector3 v6;

            tessellationlvl += tesselationLevel;

            for (int i = 0; i < tesselationLevel; i++)
            {

                int nfaces = icosqueue.Count;

                for (int j = 0; j < nfaces; j++)
                {
                    face = icosqueue.Dequeue();
                    v1 = face.Item1;
                    v2 = face.Item2;
                    v3 = face.Item3;
                    v4 = v1 + v2;
                    v4 = Vector3.Multiply(v4, (1.0f / (float)v4.Length()));
                    v5 = v2 + v3;
                    v5 = Vector3.Multiply(v5, (1.0f / (float)v5.Length()));
                    v6 = v3 + v1;
                    v6 = Vector3.Multiply(v6, (1.0f / (float)v6.Length()));

                    // Create 4 new faces per face
                    icosqueue.Enqueue(Tuple.Create(v1, v6, v4));
                    icosqueue.Enqueue(Tuple.Create(v6, v3, v5));
                    icosqueue.Enqueue(Tuple.Create(v6, v5, v4));
                    icosqueue.Enqueue(Tuple.Create(v4, v5, v2));
                }
            }
        }
    
    
    }
}
