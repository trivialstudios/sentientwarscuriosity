﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Toolkit.Input;
using SharpDX;
using SharpDX.Toolkit;
namespace SentientWarsCuriosity.Engine.Object
{
    using SharpDX.Toolkit.Graphics;
    using Windows.UI.Input;
    class SkyBox
    {
        private Model model;
        private Matrix world;
        private Matrix view;
        private Matrix projection;
        public Vector3 pos;
        public SentientWarsCuriosity game;
        public SkyBox(SentientWarsCuriosity game, Vector3 startpos)
        {
            model = game.Content.Load<Model>("Skybox");
            BasicEffect.EnableDefaultLighting(model, true);
            this.game = game;
            this.pos = startpos;
        }
        public void Update(GameTime gameTime)
        {
            // Rotate the cube.
            var time = (float)gameTime.TotalGameTime.TotalSeconds;
            view = game.camera.getView();
            projection = game.camera.getProjection();
            world = Matrix.Scaling(25f) * Matrix.Translation(pos);
        }
        public void Draw(GameTime gameTime)
        {
            // Draw the model
            model.Draw(game.GraphicsDevice, world, view, projection);
        }
    }
}
