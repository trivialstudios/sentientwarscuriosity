﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;

using System.Diagnostics;

namespace SentientWarsCuriosity.Engine.Object
{
    using SharpDX.Toolkit.Graphics;
    using Engine.Object.Shape;
    using Engine.Object;
    using Gameplay;

    public class Missile : MissileModel
    {
        private City origin, dest;
        private Vector3 orientation;
        private float speed = 0.15f;
        private float flightTime;
        private int rotateClockwise;

        private Explosion e;
        private Boolean exploded = false;

        public Missile(SentientWarsCuriosity game, City setorigin, City setdest) : base(game)
        {
            origin = setorigin;
            dest = setdest;
            pos = origin.getTile().getTileLoc();
            orientation = Vector3.Multiply(pos, 1.1f);
            flightTime = 0;
            double angle = Math.Acos(Vector3.Dot(Vector3.Normalize(origin.getTile().getTileLoc()), Vector3.Normalize(dest.getTile().getTileLoc())));

            e = new Explosion(game, dest.getTile().getTileLoc());

            // Find closest angle
            if (angle >= 180)
            {
                rotateClockwise = 1;
            }
            else
            {
                rotateClockwise = -1;
            }
        }

        public override void Update(GameTime gameTime)
        {
            flight(gameTime);

            float dist = Vector3.Distance(pos, Vector3.Multiply(dest.getTile().getTileLoc(),1.25f));
            
            if (dist < 0.35)
            {
                pos = new Vector3(100, 100, 100);
                orientation = new Vector3(0, 0, 0);
                if (!exploded)
                {
                    game.sounds.PlayWave("Explosion");
                    game.addModel(e);
                    exploded = true;
                }
                if (dist < 0.15)
                {
                    onColision();
                }
            }

            Matrix rotation = Orientate();

            Debug.WriteLine(pos.ToString());

            float time = (float) gameTime.TotalGameTime.TotalSeconds;

            setWorld(Matrix.Scaling(1, -1, 1) * Matrix.Scaling(0.1f) * rotation * Matrix.Translation(pos) * Matrix.RotationY(time / 10));
            setProjection(this.game.camera.getProjection());
            setView(this.game.camera.getView());
        }

        private void flight(GameTime gameTime)
        {
            //pos = Vector3.Normalize(dest.getTile().getTileLoc() - origin.getTile().getTileLoc());
            //Matrix.RotationAxis(
            flightTime = flightTime + (float) gameTime.ElapsedGameTime.TotalSeconds;

            pos = (Matrix.Translation(origin.getTile().getTileLoc()) * Matrix.RotationAxis(Vector3.Cross(dest.getTile().getTileLoc(), origin.getTile().getTileLoc()), rotateClockwise * flightTime * speed)).TranslationVector;
            pos = Vector3.Normalize(pos) + Vector3.Multiply(pos, 0.20f);

            orientation = (Matrix.Translation(origin.getTile().getTileLoc()) * Matrix.RotationAxis(Vector3.Cross(dest.getTile().getTileLoc(), origin.getTile().getTileLoc()), rotateClockwise * flightTime * speed + 0.25f)).TranslationVector;
            orientation = Vector3.Normalize(orientation) + Vector3.Multiply(orientation, 0.20f);

        }

        private Matrix Orientate()
        {
            Vector3 up = new Vector3(0.0f, 1.0f, 0.0f);
            Vector3 nn = Vector3.Cross(up, orientation - pos);
            float cosMalpha = Vector3.Dot(up, (orientation - pos)) / ((orientation - pos).Length());
            float sinMalpha = (nn.Length()) / ((orientation - pos).Length());
            float alpha = (float)Math.Atan2(sinMalpha, cosMalpha);
            Vector3 axis = nn / nn.Length();
            Matrix rotationXYZ = Matrix.RotationAxis(axis, alpha);
            return rotationXYZ;
        }

        public void onColision()
        {
            dest.gotNuked();
            if (exploded)
            {
                game.removeModel(e);
            }
            game.removeMissile(this);
        }        

        public static void launchMissile(SentientWarsCuriosity game, City setorigin, City setdest)
        {
            // fire missile to destination.
            game.sounds.PlayWave("Alert");
            Missile m = new Missile(game, setorigin, setdest);
            // Added incoming and launched
            game.addMissile(m);
        }

    }
}
