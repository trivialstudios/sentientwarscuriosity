﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;

namespace SentientWarsCuriosity.Engine
{
    public static class Calculus
    {
        public static Vector3 cartesianToSpherical(Vector3 cart)
        {
            Vector3 polar = new Vector3();
            polar.X = (float)Math.Sqrt(cart.X * cart.X + cart.Y * cart.Y + cart.Z * cart.Z);
            polar.Y = (float)Math.Acos(cart.Z / polar.X);
            polar.Z = (float)Math.Atan2(cart.Y, cart.X);
            return polar;
        }
        public static Vector3 sphericalToCartesian(Vector3 polar)
        {
            Vector3 cart = new Vector3();
            cart.X = polar.X * (float)Math.Sin(polar.Y) * (float)Math.Cos(polar.Z);
            cart.Y = polar.X * (float)Math.Sin(polar.Y) * (float)Math.Sin(polar.Z);
            cart.Z = polar.X * (float)Math.Cos(polar.Y);
            return polar;
        }

        public static float getSphericalDist(Vector3 startPos, Vector3 endPos)
        {
            //note: this is a rough estimate
            float avgZ = startPos.X / endPos.X;
            float dTheta = endPos.Y - startPos.Y;
            float dPhi = endPos.Z - startPos.Z;
            return avgZ * avgZ * (float)Math.Sqrt(((float)(dTheta * dTheta + dPhi * dPhi)));
        }
    }
}
