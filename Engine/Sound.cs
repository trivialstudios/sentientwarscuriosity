﻿using SharpDX.IO;
using SharpDX.Multimedia;
using SharpDX.XAudio2;
using System.Collections.Generic;
using System.Linq;

namespace SentientWarsCuriosity.Engine
{
    public class Sound
    {
        private XAudio2 xAudio;
        private List<Wave> waves = new List<Wave>();

        public Sound()
        {
            xAudio = new XAudio2();
            var mastering = new MasteringVoice(xAudio);
            mastering.SetVolume(1, 0);
            xAudio.StartEngine();
        }

        public void LoadWave(string path, string key)
        {
            var buffer = GetBuffer(path);
            waves.Add(new Wave { Buffer = buffer, Key = key });
        }

        public void PlayWave(string key)
        {
            var wave = waves.FirstOrDefault(x => x.Key == key);
            if (wave != null)
            {
                var voice = new SourceVoice(xAudio, wave.Buffer.WaveFormat, true);
                voice.SubmitSourceBuffer(wave.Buffer, wave.Buffer.DecodedPacketsInfo);
                voice.Start();
            }
        }

        private AudioBufferAndMetaData GetBuffer(string soundfile)
        {
            var nativefilestream = new NativeFileStream(soundfile, NativeFileMode.Open, NativeFileAccess.Read, NativeFileShare.Read);
            var soundstream = new SoundStream(nativefilestream);
            var buffer = new AudioBufferAndMetaData
            {
                Stream = soundstream.ToDataStream(),
                AudioBytes = (int)soundstream.Length,
                Flags = BufferFlags.EndOfStream,
                WaveFormat = soundstream.Format,
                DecodedPacketsInfo = soundstream.DecodedPacketsInfo
            };
            return buffer;
        }

        private sealed class AudioBufferAndMetaData : AudioBuffer
        {
            public WaveFormat WaveFormat { get; set; }
            public uint[] DecodedPacketsInfo { get; set; }
        }

        private class Wave
        {
            public AudioBufferAndMetaData Buffer { get; set; }
            public string Key { get; set; }
        }
    }
}