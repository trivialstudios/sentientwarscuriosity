﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;
using Windows.UI.Input;

namespace SentientWarsCuriosity.Engine
{
    abstract public class Camera
    {
        /**
         * All external AND INTERNAL modification of these fields must occur through a method!
         */
        Matrix View;
        private Matrix Projection;
        protected SentientWarsCuriosity game;
        private Matrix cameraTransform;
        
        public float camProjection;
        public Vector3 viewPos;

        // Ensures that all objects are being rendered from a consistent viewpoint
        public Camera(SentientWarsCuriosity game, Matrix initView, Matrix initProjection)
        {
            this.game = game;
            this.View = initView;
            this.cameraTransform = Matrix.Invert(this.View);
            this.Projection = initProjection;
        }

        public Camera(SentientWarsCuriosity game)
        {
            this.game = game;
            this.viewPos = new Vector3(0, 0, -100);
            this.View = Matrix.LookAtLH(this.viewPos, new Vector3(0, 0, 0), Vector3.UnitY);
            this.cameraTransform = Matrix.Invert(this.View);
            this.camProjection = (float)Math.PI / 4.0f;
            this.Projection = Matrix.PerspectiveFovLH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f, 100.0f);
        }

        // If the screen is resized, the projection matrix will change
        abstract public void Update(GameTime gameTime);

        protected Matrix getCameraTransform()
        {
            return this.cameraTransform;
        }
        
        protected void setCameraTransform(Matrix newTransform)
        {
            this.cameraTransform = newTransform;
            this.View = Matrix.Invert(this.cameraTransform);
        }
     
        protected void setProjection(Matrix newProjection)
        {
            this.Projection = newProjection;
        }

        public Vector3 getCameraPos()
        {
            return this.cameraTransform.TranslationVector;
        }

        public virtual void scaleCamera(float scale)
        {
            setCameraTransform(getCameraTransform() * Matrix.Scaling(scale));
        }

        public Vector3 getCameraTarget()
        {
            return this.cameraTransform.Forward;
        }

        public Matrix getProjection()
        {
            return this.Projection;
        }

        public Matrix getView()
        {
            return this.View;
        }

        public virtual void OnManipulationUpdated(GestureRecognizer sender, ManipulationUpdatedEventArgs args)
        {

        }
    }
}