﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;

namespace SentientWarsCuriosity.Engine
{
    using Gameplay;
    using Engine.Object;
    using Engine.Lighting;
    using SharpDX.Toolkit.Graphics;

    /// <summary>
    /// A player is also a nation. 
    /// 
    /// </summary>
    public class Player
    {
        private List<City> city;
        private SentientWarsCuriosity game;

        private bool isHuman;
        public int playerNumber { get; set; }
        private int nCities;

        //Nation Atrributes
        private float atkRate;
        private float defRate;

        private string nation;
        private string leader;

        public Gov govType { get; set;  }
        public GovModifier govModifier { get; set; }
        public Modifier eventModifier { get; set; }
        private int eventDuration;

        private static int playerCount = 0;

        public enum Gov
        {
            DEMOCRACY,
            COMMUNISM,
            REPUBLIC,
            TECHNOLOGIST,
            CONSTITUTIONAL,
            DESPOTIC,
            ENVIRONMENTALIST,
        }   

        public Player(SentientWarsCuriosity game, int nCities, bool isHuman, string nation, string leader)
        {
            Random rand = game.rand;

            this.nCities = nCities;
            this.city = new List<City>();
            this.game = game;
            this.playerNumber = playerCount;
            this.isHuman = isHuman;

            this.nation = nation;
            this.leader = leader;

            this.govType = (Gov)(rand.Next() % 7);
            this.govModifier = new GovModifier(govType);
            this.eventDuration = 0;

            playerCount++;
        }

        public void init()
        {
            city.AddRange(game.tileGrid.generateCities(nCities, this, game));
            foreach (City c in city)
                game.addModel(c);
        }

        public void update(GameTime gameTime)
        {
            foreach (City c in city)
            {
                c.Update(gameTime);
            }
        }

        public List<City> getCityList()
        {
            return this.city;
        }

        public void nationEvent()
        {
            //random nation events
            if (eventDuration > 0)
                return;

            Random rand = this.game.rand;

            switch (rand.Next() % 10)
            {
                case 0:
                case 1:
                    this.election();
                    break;
                case 2:
                    this.revertEvent();
                    this.advancement();
                    this.applyEvent();
                    break;
                case 4:
                    this.revertEvent();
                    this.discovery();
                    this.applyEvent();
                    break;
                case 6:
                    this.revertEvent();
                    this.disaster();
                    this.applyEvent();
                    break;
                default:
                    break;
            }
        }

        public void election()
        {
            Random rand = this.game.rand;
            this.revertGovModifier();
            this.govType = (Gov)(rand.Next() % 7);
            this.applyGovModifier();
        }

        public enum Event
        {
            FAMINE,
            SCANDAL,
            VIRUS,
            ENVIRONMENT,
            UNREST,
            ADVANCEMENT,
            DISCOVERY,
            ENVIRONEMENT,
        };

        public void disaster()
        {
            Random rand = this.game.rand;

            switch (rand.Next() % 10)
            {
                case 0:
                    // Famine
                    this.eventModifier = new Modifier(0, 0, -0.1f, -0.05f, 0, 0, 0, 10, Event.FAMINE);
                    break;
                case 2:
                    // Government Scandal.
                    this.eventModifier = new Modifier(-0.2f, 0, 0, 0, 0, 0, 0, 5, Event.SCANDAL);
                    break;
                case 4:
                    // Technology Out of Control. 
                    if (this.govType == Gov.TECHNOLOGIST)
                    {
                        this.eventModifier = new Modifier(0, -0.5f, -0.2f, 0, -0.25f, 0, -0.2f, 5, Event.VIRUS);
                        // Nation overly dependent on technology. BIG BIG penalty!
                    }
                    else
                    {
                        // Nation somewhat dependent on technology. Less penalty.
                        this.eventModifier = new Modifier(0, -0.2f, 0, 0, -0.15f, 0, -0.1f, 8, Event.VIRUS);
                    }
                    break;
                case 6:
                    // Environmental Disaster. 
                    // TODO: Prompt User about this event manually.
                    foreach (City c in city)
                    {
                        c.population = (int)(c.population * rand.NextFloat(0.5f, 1));
                    }
                    break;
                case 8:
                    // Civil Unrest.
                    this.eventModifier = new Modifier(-0.15f, 0, 0, 0, 0, -0.15f, -0.15f, 5, Event.UNREST);
                    break;
                default:
                    break;
            }
        }

        public void advancement()
        {
            this.eventModifier = new Modifier(0, 0.20f, 0, 0, 0.15f, 0, 0, 3, Event.ADVANCEMENT);
        }

        public void discovery()
        {
            // TECH BOOST
            this.eventModifier = new Modifier(0, 0.4f, 0, 0, 0, 0, 0, 3, Event.DISCOVERY);
        }

        public void applyGovModifier()
        {
            foreach (City c in city)
            {
                c.happyRate += govModifier.happyRate;
                c.sciRate += govModifier.sciRate;
                c.healthRate += govModifier.healthRate;
                c.prodRate += govModifier.prodRate;
            }
            this.atkRate += govModifier.attackRate;
            this.defRate += govModifier.defendRate;

        }

        public void revertGovModifier()
        {
            foreach (City c in city)
            {
                c.happyRate -= govModifier.happyRate;
                c.sciRate -= govModifier.sciRate;
                c.healthRate -= govModifier.healthRate;
                c.prodRate -= govModifier.prodRate;
            }
            this.atkRate -= govModifier.attackRate;
            this.defRate -= govModifier.defendRate;

        }

        public void applyEvent()
        {
            this.eventDuration = eventModifier.duration;

            foreach (City c in city)
            {
                c.happyRate += eventModifier.happyRate;
                c.sciRate += eventModifier.sciRate;
                c.healthRate += eventModifier.healthRate;
                c.prodRate += eventModifier.prodRate;
                c.popRate += eventModifier.prodRate;
            }
            this.atkRate += eventModifier.attackRate;
            this.defRate += eventModifier.defendRate;

            
        }

        public void revertEvent()
        {
            foreach (City c in city)
            {
                c.happyRate -= eventModifier.happyRate;
                c.sciRate -= eventModifier.sciRate;
                c.healthRate -= eventModifier.healthRate;
                c.prodRate -= eventModifier.prodRate;
                c.popRate -= eventModifier.prodRate;
            }
            this.atkRate -= eventModifier.attackRate;
            this.defRate -= eventModifier.defendRate;
        }

        public string getNationName()
        {
            return govModifier.name + " of " + this.nation;
        }

        public string getPlayerName()
        {
            return govModifier.leaderTitle + this.leader;
        }

        public struct GovModifier
        {
            public float happyRate;
            public float sciRate;
            public float healthRate;
            public float prodRate;
            public float attackRate;
            public float defendRate;

            public string name;
            public string leaderTitle;

            public GovModifier(Gov govType)
            {
                switch (govType)
                {
                    case Gov.COMMUNISM:
                        this.name = "People's Republic";
                        this.leaderTitle = "Chairman ";
                        this.attackRate = 0.10f;
                        this.defendRate = 0.25f;
                        this.happyRate = -0.05f;
                        this.sciRate = 0.10f;
                        this.prodRate = -0.10f;
                        this.healthRate = 0;
                        break;
                    case Gov.CONSTITUTIONAL:
                        this.name = "Kingdom";
                        this.leaderTitle = "Prime Minister ";
                        this.attackRate = 0.05f;
                        this.defendRate = 0.00f;
                        this.happyRate = 0.15f;
                        this.sciRate = 0.10f;
                        this.prodRate = 0.15f;
                        this.healthRate = 0.05f;
                        break;
                    case Gov.DEMOCRACY:
                        this.name = "Democratic Republic";
                        this.leaderTitle = "President ";
                        this.attackRate = 0.00f;
                        this.defendRate = 0.00f;
                        this.happyRate = 0.10f;
                        this.sciRate = 0.15f;
                        this.prodRate = 0.15f;
                        this.healthRate = 0.1f;
                        break;
                    case Gov.DESPOTIC:
                        this.name = "Despotic State";
                        this.leaderTitle = "Despot ";
                        this.attackRate = 0.20f;
                        this.defendRate = -0.05f;
                        this.happyRate = -0.10f;
                        this.sciRate = -0.10f;
                        this.prodRate = 0.20f;
                        this.healthRate = -0.05f;
                        break;
                    case Gov.ENVIRONMENTALIST:
                        this.name = "Green State";
                        this.leaderTitle = "Green Thumb ";
                        this.attackRate = -0.30f;
                        this.defendRate = -0.20f;
                        this.happyRate = 0.25f;
                        this.sciRate = 0.3f;
                        this.prodRate = -0.25f;
                        this.healthRate = 0.25f;
                        break;
                    case Gov.REPUBLIC:
                        this.name = "Republic";
                        this.leaderTitle = "Chancellor ";
                        this.attackRate = 0.15f;
                        this.defendRate = 0.15f;
                        this.happyRate = 0.0f;
                        this.sciRate = 0.10f;
                        this.prodRate = 0.15f;
                        this.healthRate = 0.05f;
                        break;
                    case Gov.TECHNOLOGIST:
                        this.name = "Futuristic State";
                        this.leaderTitle = "Honorary Professor ";
                        this.attackRate = -0.5f;
                        this.defendRate = 0.5f;
                        this.happyRate = -0.1f;
                        this.sciRate = 0.4f;
                        this.prodRate = 0.05f;
                        this.healthRate = 0.15f;
                        break;
                    default:
                        this.name = "ERROR";
                        this.leaderTitle = "ERROR";
                        this.attackRate = -0.5f;
                        this.defendRate = 0.5f;
                        this.happyRate = -0.1f;
                        this.sciRate = 0.4f;
                        this.prodRate = 0.05f;
                        this.healthRate = 0.15f;
                        break;
                }
            }
        }

        public struct Modifier
        {
            public float happyRate;
            public float sciRate;
            public float healthRate;
            public float popRate;
            public float prodRate;
            public float attackRate;
            public float defendRate;
            public int duration;
            public Event type;

            public Modifier(float happyRate, float sciRate, float healthRate, float popRate, float prodRate, float atkRate, float defRate, int duration, Event type)
            {
                this.happyRate = happyRate;
                this.sciRate = sciRate;
                this.healthRate = healthRate;
                this.popRate = popRate;
                this.prodRate = prodRate;
                this.attackRate = atkRate;
                this.defendRate = defRate;
                this.duration = duration;
                this.type = type;
            }
        }
    }
}
